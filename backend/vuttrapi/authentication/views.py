from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from users.serializers import UserLoginSerializer
from rest_framework import status
from rest_framework.response import Response
import jwt
# Create your views here.

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.tokens import RefreshToken
'''
class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_tokens_for_user(user):
        print(user)
        refresh = RefreshToken.for_user(user)

        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

class LoginView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer
'''
class LoginView(CreateAPIView):
    serializer_class=UserLoginSerializer

    def post(self, request):
        data = request.data
        username = data.get('username')
        password = data.get('password')

        user = auth.authenticate(username=username, password=password)

        if user:
            auth_token = jwt.encode({'username': user.username}, settings.SIMPLE_JWT['SIGNING_KEY'])

            data = {
                'token': auth_token
            }
            return Response(data, status=status.HTTP_200_OK)
        return Response({'mensagem': 'Credenciais inválidas.'}, status=status.HTTP_401_UNAUTHORIZED)
