from django.urls import path
from authentication.views import LoginView

urlpatterns = [
    path('signin/', LoginView.as_view())
]