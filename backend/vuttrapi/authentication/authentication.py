import jwt
from rest_framework import authentication
from rest_framework.response import Response
from django.conf import settings
from django.contrib.auth import settings

class JWTAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        auth_data = authentication.get_authorization_header(request)

        if not auth_data:
            return None

        prefix, token = auth_data.decode('utf-8').split(' ')

        try:
            payload = jwt.decode(token, settings.SIMPLE_JWT['SIGNING_KEY'])
            user = User.object.get(username=payload['username'])
            return (user, token)
        except jwt.DecodeError as identifier:
            raise exceptions.AuthenticationFailed('Erro ao decodificar o token')
        except jwt.ExpiredSignaturaError as identifier:
            raise exceptions.AuthenticationFailed('Sua sessão foi expirada, por favor realize login novamente')

        return super().authentication(request)