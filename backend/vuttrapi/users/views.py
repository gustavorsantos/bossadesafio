from django.shortcuts import render
from users.serializers import UserSerializer, UserRegistrationSerializer
from rest_framework import viewsets
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated, AllowAny
from vuttrapi.permissions import IsSelfOwnerOrAdmin
from rest_framework.response import Response

# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.request.method in ['PUT', 'PATCH', 'DELETE']:
            return [IsSelfOwnerOrAdmin()]
        return [AllowAny()]

    def create(self, request):
        serializer = UserRegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        model_serializer = UserSerializer(data=serializer.data)
        model_serializer.is_valid(raise_exception=True)

        model_serializer.save(user=request.user)

        return Response({'mensagem':'Usuário cadastrado com sucesso!'}, status=status.HTTP_201_CREATED)