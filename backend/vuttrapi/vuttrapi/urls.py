from django.contrib import admin
from django.urls import path, include
from users.urls import router as users_routers

from authentication.views import LoginView
from vuttr.urls import router as vuttr_routers

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

api_routers = users_routers.urls + vuttr_routers.urls
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_routers)),
    path('auth/login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
