from rest_framework import serializers
from vuttr.models import Tool, Tag

class ToolSerializer(serializers.ModelSerializer):
    tags = serializers.SlugRelatedField(many=True, read_only=True, slug_field='name')
    class Meta:
        model = Tool
        fields = ['id', 'user', 'title', 'link', 'description', 'tags']
        extra_kwargs = {'user': {'read_only': True}, }

        
class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ['id', 'name']
    
    