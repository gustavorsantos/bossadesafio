from vuttr.views import ToolViewSet, TagViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'tools', ToolViewSet, basename='tools')
router.register(r'tags', TagViewSet, basename='tags')