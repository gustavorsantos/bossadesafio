from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Tool(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=30)
    link = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    tags = models.ManyToManyField('Tag', through='ToolTag')

class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=15)

class ToolTag(models.Model):
    tool = models.ForeignKey(Tool, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)