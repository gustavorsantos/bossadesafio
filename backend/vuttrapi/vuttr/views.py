from django.shortcuts import render
from vuttr.serializers import ToolSerializer, TagSerializer
from rest_framework import viewsets
from vuttr.models import Tool, Tag
from rest_framework.permissions import IsAdminUser, AllowAny, IsAuthenticated
from vuttrapi.permissions import IsSelfOwnerOrAdmin, IsOwnerOrAdmin
from rest_framework.response import Response
from rest_framework import status


# Create your views here.
class ToolViewSet(viewsets.ModelViewSet):
    queryset = Tool.objects.all()
    serializer_class = ToolSerializer

    def create(self, request):
        tags = []
        request_tags = request.data['tags']
        for tag in request_tags:
            search_tag = Tag.objects.filter(name=tag)
            if(not search_tag.exists()):
                tags.append(Tag.objects.create(name=tag))
            else:
                tags.append(search_tag[0])
        serializer = ToolSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        tool = serializer.save(user=request.user)
        for tag in tags:
            tool.tags.add(tag)
        
        return Response({'mensagem':'Ferramenta adiciona com sucesso!'}, status=status.HTTP_201_CREATED)
        
    def get_permissions(self):
        if self.request.method in ['PATCH', 'PUT', 'DELETE']:
            return [IsOwnerOrAdmin()]
        if self.request.method == 'POST':
            return [IsAuthenticated()]
        return [AllowAny()]

class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer

    def get_permissions(self):
        if self.request.method in ['PUT', 'PATCH', 'DELETE']:
            return [IsAdminUser()]
        if self.request.method == 'POST':
            return [IsAuthenticated()]
        return [AllowAny()]